from fastapi import APIRouter
from fastapi import  Path, Query, Depends
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middleware.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get('/movies', tags=['Movies'], response_model=List[Movie], status_code=200)
def get_movies() -> List[Movie]:
	db = Session()
	result = MovieService(db).get_movies()
	return JSONResponse(content= jsonable_encoder(result), status_code=200)

@movie_router.get('/movies/{id}', tags=['Movies'], response_model=Movie, status_code=200)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
	db = Session()
	result = MovieService(db).get_movie(id)
	if not result: 
		return JSONResponse(status_code=404, content={"message": "No encontrado."})
	return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/category/', tags=['Movies'], response_model=List[Movie], status_code=200)
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
	db = Session()
	result = MovieService(db).get_movies_by_category(category)
	if not result:
		return JSONResponse(status_code=404, content={"message":"Ninguna pelicula encontrada"})
	return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies/create/', tags=['Movies'], response_model=dict, status_code=200)
def create_movie(movie: Movie) -> dict:
	db = Session()
	MovieService(db).create_movie(movie)
	return JSONResponse(status_code=200, content={"message": "Se ha registrado la pelicula"})

@movie_router.delete('/movies/delete/', tags=['Movies'], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
	db = Session()
	result = db.query(MovieModel).filter(MovieModel.id == id).first()
	if not result:
		return JSONResponse(status_code=404, content={'message': "No encontrado."})
	MovieService(db).delete_movie(id)
	return JSONResponse(status_code=200, content={"message":"Se ha eliminado la pelicula."})
	
@movie_router.put('/movies/update/', tags=['Movies'], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
	db = Session()
	result = MovieService(db).get_movie(id)
	if not result:
		return JSONResponse(status_code=404, content={"message":"No encontrada"})	
	MovieService(db).update_movie(id, movie)
	return JSONResponse(status_code=200, content={"message": "Se ha modificado la pelicula"})