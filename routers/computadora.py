from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computadora import Computadora as ComputadoraModel
from fastapi.encoders import jsonable_encoder
from middleware.jwt_bearer import JWTBearer

computadora_router = APIRouter()

class Computadora(BaseModel):
	id: Optional[int] = None
	marca: str = Field(min_length=2, max_length=15)
	modelo: str = Field(min_length=5, max_length=16)
	color: str = Field(min_length=4, max_length=15)
	ram: int = Field(le=64)
	almacenamiento: str = Field(min_length=5, max_length=15)

	class Config:
		json_schema_extra = {
			"example": {
				"marca": "Marca de la computadora",
				"modelo": "Modelo de la computadora",
				"color": "Color de la computadora",
				"ram": 12,
				"almacenamiento": "500 GB"
			}
		}

id = 0
computadoras = []

@computadora_router.get('/computadoras/', tags=['Computadoras'], response_model=List[Computadora], status_code=200)
def get_Computadoras() -> List[Computadora]:
	db = Session()
	result = db.query(ComputadoraModel).all()
	return JSONResponse(content=jsonable_encoder(result), status_code=200)

@computadora_router.get('/computadoras/{id}', tags=['Computadoras'], status_code=200)
def get_Computadora(id: int = Path(ge=1, le=100)):
	db = Session()
	result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
	if not result:
		return JSONResponse(status_code=404, content={"message":"No hay computadora registrada con ese id."})
	return JSONResponse(status_code=200, content=jsonable_encoder(result))
	# for item in computadoras:
	# 	if item["id"] == id:
	# 		return JSONResponse(status_code=200, content=item)
	# return JSONResponse(content={"message":"No hay computadora registrada con ese id"}, status_code=400)

@computadora_router.get('/computadoras/marcas/', tags=['Computadoras'], response_model=List[Computadora], status_code=200)
def get_Computadora_By_Marca(marca: str = Query(min_length=2, max_length=15)):
	db = Session()
	result = db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
	if not result:
		return JSONResponse(status_code=404, content={"message":"No hay computadoras registradas con esa marca."})
	return JSONResponse(status_code=200, content=jsonable_encoder(result))
	# paJSON = [item for item in computadoras if item["marca"] == marca]
	# return JSONResponse(content=paJSON, status_code=200)

@computadora_router.post('/computadora/', tags=['Computadoras'], response_model=dict, status_code=200)
def create_computadora(computadora: Computadora) -> dict:
	db = Session()
	nueva_computadora = ComputadoraModel(**computadora.model_dump())
	db.add(nueva_computadora)
	db.commit()
	return JSONResponse(content={"message":"Se ha registrado una nueva computadora."})
	# global id
	# id += 1
	# computadora.id = id
	# computadora_dict = computadora.dict()
	# computadoras.append(computadora_dict)
	

@computadora_router.delete('/computadoras/', tags=['Computadoras'], response_model=dict, status_code=200)
def delete_computadora(id: int) -> dict:
	db = Session()
	result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
	if not result:
		return JSONResponse(status_code=404, content={"message":"No hay computadora registrada con ese id."})
	db.delete(result)
	db.commit()
	return JSONResponse(status_code=200, content={"message":"Computadora eliminada exitosamente."})
	# for item in computadoras:
	# 	if item['id'] == id:
	# 		computadoras.remove(item)
	# 		return JSONResponse(content={"message":"Se elimino el registro exitosamente"})
	# return JSONResponse(content={"message":"No hay registro con ese id."})

@computadora_router.put('/computadoras/', tags=['Computadoras'], response_model=dict, status_code=200)
def edit_computadora(idBusqueda: int, computadora: Computadora):
	db = Session()
	result = db.query(ComputadoraModel).filter(ComputadoraModel.id == idBusqueda).first()
	if not result:
		return JSONResponse(status_code=404, content={"message":"No hay computadora registrada con ese id"})
	result.marca = computadora.marca
	result.modelo = computadora.modelo
	result.color = computadora.color
	result.ram = computadora.ram
	result.almacenamiento = computadora.almacenamiento
	db.commit()
	return JSONResponse(status_code=200, content={"message":"Se realizo la actualización correctamente."})
	# for item in computadoras:
	# 	if item['id'] == idBusqueda:
	# 		item["marca"] = computadora.marca
	# 		item["modelo"] = computadora.modelo
	# 		item["color"] = computadora.color
	# 		item["ram"] = computadora.ram
	# 		item["almacenamiento"] = computadora.almacenamiento
	# 		return JSONResponse(content={"message":"Se actualizo correctamente"}, status_code=200)
	# return JSONResponse(content={"message":"No hay registro con ese id."}, status_code=400)