from fastapi import FastAPI
from config.database import engine, Base
from middleware.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router
from routers.computadora import computadora_router

app = FastAPI()
app.title = "Mi primera chamba con FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)

app.include_router(user_router)
app.include_router(computadora_router)

Base.metadata.create_all(bind=engine)