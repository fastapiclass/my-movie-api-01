from pydantic import BaseModel, Field
from typing import Optional

#ESQUEMA
class Movie(BaseModel):
	id: Optional[int] = None
	title: str = Field(min_length=5, max_length=25)
	overview : str = Field(min_length=15, max_length=50)
	year: int = Field(le=2024)
	rating: float = Field(ge=1, le=10)
	category: str = Field(min_length=5, max_length=15)

#Tipo de parametro: Ruta: Estan en los parametros de la funcion
#Tipo de parametro: Query:

	class Config:
		json_schema_extra = {
			"example": {
				"id": 1,
				"title": "Titulo de Pelicula",
				"overview": "Reseña de pelicula",
				"year": 1234,
				"rating": 1.1,
				"category": "Genero de Pelicula"
			}
		}